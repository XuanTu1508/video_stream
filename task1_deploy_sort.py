import warnings
warnings.filterwarnings('ignore')
import os
from sort import Sort
import cv2
from imutils.video import FPS
import numpy as np
import tensorflow as tf
import align.detect_face as detect_face
import flask
from flask import Flask, render_template, Response

paths = "E:/project_coffee-store/file22"
if not os.path.exists(paths):
    os.mkdir(paths)

app = Flask(__name__)
APP_ROOT = os.path.dirname(os.path.abspath(__file__))
UPLOAD_FOLDER = os.path.join(APP_ROOT, 'static')
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER
@app.route("/")
@app.route("/index")
def index():
    return flask.render_template('index.html')

#@app.route("/live", methods=['GET', 'POST'])
def make_prediction():
    cin = 0
    margin = 10
    face_score_threshold = 0.80
    fps = FPS().start()
    global graph
    with graph.as_default():
    #with tf.Graph().as_default():
        with tf.Session(config=tf.ConfigProto(gpu_options=tf.GPUOptions(allow_growth=True),
                                              log_device_placement=False)) as sess:
            p_net, r_net, o_net = detect_face.create_mtcnn(sess, os.path.join(os.path.dirname(os.path.abspath(__file__)), "align"))
            minsize = 60
            #threshold = [0.6, 0.7, 0.7]
            threshold = [0.6, 0.75, 0.85]
            factor = 0.709
            total_face = []
            colours = np.random.rand(32, 3)
            capture = cv2.VideoCapture(videoFile)
            while True:
                ret, frame = capture.read()
                if ret != True:
                    break
                if ret:
                    frame = cv2.resize(frame, (640, 480))
                    r_g_b_frame = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
                    bounding_boxes, point = detect_face.detect_face(r_g_b_frame, minsize, p_net, r_net, o_net, threshold, factor)
                    face_sums = bounding_boxes.shape[0]
                    try:
                        if face_sums > 0:
                            face_list = []
                            for i, face_position in enumerate(bounding_boxes):
                                score = round(bounding_boxes[i, 4], 6)
                                if score > face_score_threshold:
                                    det = np.squeeze(bounding_boxes[i, 0:4])
                                    face_list.append(face_position)
                                    x1 = int(det[0] - margin)
                                    y1 = int(det[1] - margin)
                                    x2 = int(det[2] + margin)
                                    y2 = int(det[3] + margin)
                                    face = frame[y1:y2, x1:x2]
                                    final_faces = np.array(face_list)
                    except:
                        continue

                    trackers = tracker.update(bounding_boxes)
                    cin += 1
                    for tracking in trackers:
                        tracking = tracking.astype(np.int32)
                        x1 = int(tracking[0])
                        y1 = int(tracking[1])
                        x2 = int(tracking[2])
                        y2 = int(tracking[3])

                        if bounding_boxes != []:
                            number_face = int(tracking[4])
                            total_face.append(number_face)
                            cv2.rectangle(frame, (x1, y1), (x2, y2), colours[number_face % 32, :] * 255, 3)
                            face2 = frame[y1:y2, x1:x2]
                            objectID = 'ID_%d' % (number_face)
                            cin += 1
                            path2 = f"{paths}/{objectID}"
                            if not os.path.exists(path2):
                                os.mkdir(path2)
                            Visitor = f"{path2}/{cin}.jpg"
                            cv2.imwrite(Visitor, face2)

                            cv2.putText(frame, 'ID : %d' % (number_face), (x1 - 10, y1 - 10),
                                        cv2.FONT_HERSHEY_SIMPLEX,
                                        0.75,
                                        colours[number_face % 32, :] * 255, 2)

                        else:
                            tracking.pop(4)
                    value = np.array(total_face)
                    value = np.unique(value)

                #cv2.imshow("Frame", frame)
                key = cv2.waitKey(1) & 0xFF
                if key == ord("q"):
                    break
                elif cin > 100:
                    continue
                fps.update()
                fps.stop()
                print("elapsed time: {:.2f}".format(fps.elapsed()))
                print("approx. FPS: {:.2f}".format(fps.fps()))


                
                

                
            print("Total", len(value))
            total = len(value)

            
            cv2.imwrite('demo.jpg', frame)
            yield (b'--frame\r\n'
                        b'Content-Type: image/jpeg\r\n\r\n' + open('demo.jpg', 'rb').read() + b'\r\n')
            #return render_template('index.html', total = total)
            yield render_template('index.html', total = total)
            capture.release()
            cv2.destroyAllWindows()

@app.route('/video_feed', methods=['GET', 'POST'])
def video_feed():
    return Response(make_prediction(),
                   mimetype='multipart/x-mixed-replace; boundary=frame')


if __name__ == '__main__':
    tracker = Sort()
    videoFile = "E:/project_coffee-store/video/tu5.mp4"
    graph = tf.get_default_graph()
    app.run(host='0.0.0.0', port=80, debug=True)
