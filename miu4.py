# camera.py
# import the necessary packages
import cv2
from imutils.video import FPS
import dlib
from imutils import face_utils
from keras.preprocessing.image import  img_to_array
from pyimagesearch.centroidtracker_dlib import CentroidTracker
ct = CentroidTracker()

# defining face detector
face_cascade = cv2.CascadeClassifier("D:/VideoStreamingFlask-master/haarcascade_frontalface_alt2.xml")
ds_factor = 0.6
detector = dlib.get_frontal_face_detector()

class VideoCamera3(object):
    def __init__(self):
        # capturing video
        self.video = cv2.VideoCapture('D:/project_coffee-store/video/miu3.mp4')

    def __del__(self):
        # releasing camera
        self.video.release()


    def get_frame(self):
        # extracting frames
        fps = FPS().start()
        ret, frame = self.video.read()
        frame = cv2.resize(frame, None, fx=ds_factor, fy=ds_factor,
                           interpolation=cv2.INTER_AREA)

        gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

        count = 1
        rects = detector(gray, 0)

        for rect in rects:
            if rect != None:
                # name_id()
                x = int(rect.left())
                y = int(rect.top())
                w = int(rect.right() - x)
                h = int(rect.bottom() - y)

                (x1, y1, w1, h1) = face_utils.rect_to_bb(rect)
                try:
                    if w > 70:
                        # for rect in rects:
                        # (x, y, w, h) = face_utils.rect_to_bb(rect)
                        cv2.rectangle(frame, (x, y), (x + w, y + h), (255, 0, 255), 2)
                        face = frame[int(y):int(y + h), int(x):int(x + w)]

                        face = img_to_array(face)

                except TypeError:
                    continue

        objects = ct.update(rects)
        print(objects)

        for (objectID, centroid) in objects.items():

            text = "ID_{}".format(objectID)
            cv2.putText(frame, text, (int(centroid[0]) + 85, int(centroid[1] - 50)), cv2.FONT_HERSHEY_SIMPLEX, 0.75,
                        (127, 127, 11), 1)




        fps.update()
        fps.stop()
        print("elapsed time: {:.2f}".format(fps.elapsed()))
        print("approx. FPS: {:.2f}".format(fps.fps()))

        ret, jpeg = cv2.imencode('.jpg', frame)
        return jpeg.tobytes()